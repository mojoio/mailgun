export interface IMailgunMessage {
  Received: string;
  From: string;
  'X-Envelope-From': string;
  recipients: string;
  'X-Google-Dkim-Signature': string;
  To: string;
  'message-headers': [[string, string]];
  'Dkim-Signature': string;
  'content-id-map': any;
  subject: 'test2';
  'stripped-html': string;
  'X-Mailgun-Incoming': 'Yes';
  'X-Received': string;
  'X-Gm-Message-State': string;
  'body-plain': string;
  attachments: Array<{
    url: string;
    'content-type': string;
    name: string;
    size: number;
  }>;
  'body-html': string;
  'Mime-Version': string;
  Date: string;
  'Message-Id': string;
  'Content-Type': string;
  'X-Google-Smtp-Source': string;
  Subject: string;
}
